/* eslint-disable @typescript-eslint/no-var-requires */

const withStylus = require("@zeit/next-stylus")
const withCss = require("@zeit/next-css")

module.exports = withStylus(withCss())
