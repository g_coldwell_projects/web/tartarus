import { Theme } from "../../styling/styled_components/theming/theme"

declare module "styled-components" {
	// This interface must not be turned into type declaration because TypeScript's declaration merging cannot merge interface and type
	// eslint-disable-next-line @typescript-eslint/no-empty-interface
	export interface DefaultTheme extends Theme {}
}
