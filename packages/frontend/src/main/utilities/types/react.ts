import { Attributes } from "react"

export type WithKey<T = {}, TKey = Attributes["key"]> = T & {
	key: TKey
}
