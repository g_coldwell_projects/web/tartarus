export const tagArg = <TArg, TResult>(
	fun: (first: TemplateStringsArray, ...rest: TArg[]) => TResult,
	arg: TArg,
) => fun`${arg}`
