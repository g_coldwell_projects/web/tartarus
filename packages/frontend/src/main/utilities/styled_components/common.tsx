import { letS } from "../scope_functions"
import React from "react"
import styled, {
	DefaultTheme,
	FlattenInterpolation,
	StyledComponent,
	StyledComponentPropsWithRef,
	ThemedStyledProps,
} from "styled-components"

export const componentForStyle = <
	TElementType extends
		| keyof JSX.IntrinsicElements
		| React.ComponentType<
				StyledComponentPropsWithRef<TElementType> &
					FlattenInterpolation<
						ThemedStyledProps<TProps, DefaultTheme>
					>
		  > = "div",
	TProps extends {} = {}
>(
	elementType: TElementType,
	style: StyledComponentPropsWithRef<TElementType> &
		FlattenInterpolation<ThemedStyledProps<TProps, DefaultTheme>>,
): StyledComponent<TElementType, DefaultTheme, TProps, never> =>
	styled(elementType)`
		${style}
	`

type MediaQueryConditions = {
	minViewportWidth?: string
	maxViewportWidth?: string
}

const mediaQueryConditionTypeMap: Record<keyof MediaQueryConditions, string> = {
	minViewportWidth: "min-width",
	maxViewportWidth: "max-width",
}

export const createMediaQuery = (conditionsObj: MediaQueryConditions) =>
	letS(
		Object.entries(conditionsObj).filter(([, value]) => value != null) as [
			keyof MediaQueryConditions,
			string,
		][],
		conditions =>
			conditions.length == 0
				? ""
				: `@media (${conditions
						.map(
							([type, value]) =>
								`${mediaQueryConditionTypeMap[type]}: ${value}`,
						)
						.join(") and (")})`,
	)
