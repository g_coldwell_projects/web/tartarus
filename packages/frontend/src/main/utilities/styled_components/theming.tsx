import {
	BoundingBox,
	CompoundBoundingBox,
} from "../../styling/styled_components/theming/models/bounding_box"
import {
	DefaultTheme,
	FlattenInterpolation,
	ThemedStyledProps,
	css,
} from "styled-components"
import { letSIfNotNull } from "../scope_functions"

export const toSc = {
	boundingBox: ({ top, right, bottom, left }: BoundingBox) =>
		`${top} ${right} ${bottom} ${left}`,
	compoundBoundingBox: ({ inner, outter }: CompoundBoundingBox) => `
		${letSIfNotNull(inner, inner => `padding: ${toSc.boundingBox(inner)};`)}
		${letSIfNotNull(outter, outter => `margin: ${toSc.boundingBox(outter)};`)}
	`,
}

export const themed = <TProps extends {} = {}>(
	fn: (
		theme: DefaultTheme,
	) => FlattenInterpolation<ThemedStyledProps<TProps, DefaultTheme>>,
) =>
	css`
		${({ theme }) => fn(theme)}
	`
