// Scope functions as described at: https://kotlinlang.org/docs/reference/scope-functions.html#function-selection

export const letS = <ObjectType, LambdaReturnType>(
	obj: ObjectType,
	lambda: (obj: ObjectType) => LambdaReturnType,
) => lambda(obj)

export const letSIfNotNull = <ObjectType, LambdaReturnType>(
	obj: ObjectType,
	lambda: (obj: NonNullable<ObjectType>) => LambdaReturnType,
	// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
) => (obj == null ? undefined : letS(obj!, lambda))

export const alsoS = <ObjectType>(
	obj: ObjectType,
	lambda: (obj: ObjectType) => void,
) => (lambda(obj), obj)
