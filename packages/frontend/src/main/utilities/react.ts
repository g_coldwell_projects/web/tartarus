import { WithKey } from "./types/react"

export const addIndexKey = <TElement>(
	array: TElement[],
): WithKey<TElement, number>[] =>
	array.map((element, index) => ({ ...element, key: index }))
