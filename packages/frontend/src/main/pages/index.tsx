import { FullpageBanner } from "../components/fullpage_banner"
import { PageBase } from "../components/page_base"
import { PageContent } from "../styling/styled_components/common_components"
import React from "react"
import styled from "styled-components"

const Banner = styled.div`
	filter: saturate(1.5);
`

export const IndexPage: React.FC = () => (
	<PageBase>
		<Banner>
			<FullpageBanner
				underHeader
				image={{
					srcUrl:
						"https://media-exp1.licdn.com/dms/image/C4D1BAQEWV3pPfdc-qg/company-background_10000/0?e=2159024400&v=beta&t=0JtSb3zf9AQ9L-pwooJeoDbwVEs-kTl_UPEjTHQo_F4",
					altText: "",
				}}
			/>
		</Banner>
		<PageContent>Hello world!</PageContent>
	</PageBase>
)
