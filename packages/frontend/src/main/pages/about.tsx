import {
	Center,
	Heading,
	PageContent,
	Text,
} from "../styling/styled_components/common_components"
import { ImageView } from "../components/model_views/image"
import { PageBase } from "../components/page_base"
import React from "react"

export const AboutPage: React.FC = () => (
	<PageBase>
		<ImageView image={{ srcUrl: "/static/images/headings/test.png" }} />
		<PageContent>
			<Center>
				<Heading.primary>O nás</Heading.primary>
				<Text>
					Prazdroj je české označení piva v současné době vyráběného
					pod obchodním názvem Pilsner Urquell v pivovaru Plzeňský
					Prazdroj, a. s. Jde nejen o typický, ale i historický vzor
					piv plzeňského typu. Vůbec poprvé na celém světě byl tento
					druh piva uvařen 5. 10. 1842 v plzeňském pivovaru německým
					sládkem Josefem Grollem. Pilsner Urquell byl vůbec první
					světlý ležák, čímž byl inspirací pro více než dvě třetiny
					dnes vyráběného piva na světě.
				</Text>
			</Center>
		</PageContent>
	</PageBase>
)
