import "react-vertical-timeline-component/style.min.css"
import {
	Center,
	Heading,
	PageContent,
	Text,
} from "../styling/styled_components/common_components"
import { PageBase } from "../components/page_base"
import { VerticalTimeline } from "../components/vertical_timeline"
import { addIndexKey } from "../utilities/react"
import React from "react"
import styled from "styled-components"

const TestIconContainer = styled.img`
	width: 100%;
	height: 100%;
	object-fit: cover;
	border-radius: 50%;
`

const TestIcon = () => (
	<TestIconContainer src="https://lh3.googleusercontent.com/proxy/N6EiW9QdKpTIKi_cGyXpACyR3KQsZBcZbqpm4_A83bdwkXTBwTPH5aGSF0qHXnyBKS__ESEH5C8wikD-i2wl5EIiplQqqc2L5Ky-FypX" />
)

export const HistoryPage: React.FC = () => (
	<PageBase>
		<PageContent>
			<Center>
				<VerticalTimeline
					events={addIndexKey([
						{
							content: (
								<>
									<Heading.primary>
										Udělení várečného práva
									</Heading.primary>
									<Text>
										Právo várečné udělil 260 plzeňským
										občanům již Václav II. v roce 1295. Z
										roku 13077 pochází zmínka o pivovaru,
										který Wolfram Zwinilinger odkázal
										katedrále svatého Bartoloměje. Ve 14.
										století pak mohli vařit a prodávat pivo
										všichni měšťané. V roce 1501 je poprvé
										zmíněn městský pivovar.
									</Text>
								</>
							),
							icon: <TestIcon />,
							date: "1295",
						},
						{
							content: (
								<>
									<Heading.primary>
										Vznik pivovaru
									</Heading.primary>
									<Heading.secondary>
										Pilsner Urquel
									</Heading.secondary>
									<Text>
										15. října zahajuje provoz První plzeňský
										akciový pivovar (dnes Gambrinus)
										založený 20 významnými podnikateli v
										čele s Emilem Škodou. V prosinci si pak
										registruje první ochrannou známku Erste
										Pilsner Actien Brauerei – Pilsner Bier.
									</Text>
								</>
							),
							icon: <TestIcon />,
							date: "",
						},
						{
							content: (
								<>
									<Heading.primary>
										Registrace značky
									</Heading.primary>
									<Text>
										Oficiální obchodní značkou
										velkopopovického pivovaru se stává
										Kozel. Označení má původ ve zkomolenině
										dolnosaského města Einbeck, které ve 14.
										století proslulo výrobou piva spodním
										kvašením. To se tehdy šířilo do Čech pod
										názvem Ein Bock (Kozel). Emblém pivovaru
										vytvořil potulný francouzský malíř jako
										výraz díků za pohostinnost zakladetele
										Ringhoffera.
									</Text>
								</>
							),
							icon: <TestIcon />,
							date: "Date",
						},
						{
							content: (
								<>
									<Heading.primary>
										Vznik pivovaru
									</Heading.primary>
									<Text>
										František Ringhoffer ke svému
										kamenickému panství skupuje pivovar s
										mlýnem, spilkou i hvozdem a zakládá
										Pivovar Velké Popovice. První 60hl várka
										piva spatřuje světlo světa 15. prosince.
										O rok později již pivovar vystavuje 18
										tisíc hl a po rozsáhlých rekonstrukcích
										roku 1902 dokonce 80 tisíc hl piva.
									</Text>
								</>
							),
							icon: <TestIcon />,
							date: "",
						},
						{
							content: (
								<>
									<Heading.primary>
										Ochranná známka
									</Heading.primary>
									<Text>
										Spolu s tím, jak se šíří obliba
										plzeňského ležáku v Čechách i přilehlých
										zemích, objevují se jeho napodobeniny.
										Proto si v roce 1859 dává Měšťanský
										pivovar zaregistrovat značku „Pilsner
										Bier“ (Plzeňské pivo), z níž ale těží i
										ostatní piva z Plzně.
									</Text>
								</>
							),
							icon: <TestIcon />,
							date: "Date",
						},
						{
							content: (
								<>
									<Heading.primary>
										První pivo
									</Heading.primary>
									<Text>
										Pivo se v Plzni vařilo střídavě u
										jednotlivých právovárečných měšťanů.
										Kvalita byla různá, a proto se v hlavách
										plzeňských měšťanů zrodí myšlenka na
										vlastní pivovar s dobrým pivem. V novém
										Měšťanském pivovaru vaří 5. října 1842
										sládek Josef Groll první várku nového
										plzeňského piva, které brzy dobude celý
										svět.
									</Text>
								</>
							),
							icon: <TestIcon />,
							date: "really",
						},
					])}
				/>
			</Center>
		</PageContent>
	</PageBase>
)
