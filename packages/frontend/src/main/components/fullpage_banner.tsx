import { ImageModel } from "../models/image"
import { ImageView } from "./model_views/image"
import { componentForStyle } from "../utilities/styled_components/common"
import { css } from "styled-components"
import React from "react"

const containerStyle = css<{
	underHeader: boolean
}>`
	width: 100vw;
	height: 100vh;
	max-width: 100%;
	position: relative;
	${({ theme, underHeader }) =>
		underHeader ? `top: -${theme.sizes.page.header.height}` : ""};
`

const Container = componentForStyle("div", containerStyle)

export const FullpageBanner: React.FC<{
	image: ImageModel
	imageViewProps?: Partial<React.ComponentProps<typeof ImageView>>
	underHeader?: boolean
}> = ({ image, imageViewProps, underHeader }) => (
	<Container underHeader={underHeader ?? false}>
		<ImageView image={image} {...imageViewProps} />
	</Container>
)
