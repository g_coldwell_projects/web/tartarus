import { Header, SectionModel } from "./header"
import React from "react"

import "./index.styl"
import { baseTheme } from "../../styling/styled_components/theming/themes/base"
import Head from "next/head"
import styled, { ThemeProvider, css } from "styled-components"

const headerSections: SectionModel[] = [
	{ id: "home", label: "Domů", url: "/" },
	{ id: "about", label: "O nás", url: "/about" },
	{ id: "history", label: "Historie", url: "/history" },
]

const ContentContainer = styled.div`
	min-height: 100vh;
	${({ theme }) => css`
		background-color: ${theme.colors.page.background};
	`}
`

export const PageBase: React.FC = ({ children }) => (
	<>
		<Head>
			<link
				href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto+Slab|Staatliches&display=swap"
				rel="stylesheet"
			/>
		</Head>
		<ThemeProvider theme={baseTheme}>
			<ContentContainer>
				<Header sections={headerSections} />
				{children}
			</ContentContainer>
		</ThemeProvider>
	</>
)
