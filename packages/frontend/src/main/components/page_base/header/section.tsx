import { HeaderSectionModel } from "../../../models/header_section"
import { scFonts } from "../../../styling/styled_components/fonts"
import { scStyles } from "../../../styling/styled_components/common_styles"
import { themed, toSc } from "../../../utilities/styled_components/theming"
import Link from "next/link"
import React from "react"
import styled, { css } from "styled-components"

const Container = styled.a`
	${themed(
		theme => css`
			${scStyles.normalize.link}
			${scFonts.staatliches}
			font-size: 1.5rem;
			font-weight: bold;
			height: 100%;
			flex: 1 0 1;
			display: flex;
			align-items: center;
			${toSc.compoundBoundingBox(
				theme.sizes.page.header.section.boundingBox,
			)}
			transition: color 0.5s ease-in-out,
				background-color 0.25s ease-in-out;
			color: black;
			transform: translateY(0);
			&:hover {
				${({ theme }) =>
					css`
						background-color: ${theme.colors.shade.dark};
					`}
			}
		`,
	)}
`

const Label = styled.div`
	transition: transform 0.25s ease-in-out;
	transform: translateY(15%);
	${Container}:hover & {
		transform: translateY(0);
	}
`

export const Section: React.FC<{
	section: Pick<HeaderSectionModel, "label" | "url">
}> = ({ section: { label, url } }) => (
	<Link href={url} passHref>
		<Container>
			<Label>{label}</Label>
		</Container>
	</Link>
)
