import { Section } from "./section"
import { scStyles } from "../../../styling/styled_components/common_styles"
import { tagArg } from "../../../utilities/tagged_tempates"
import { themed } from "../../../utilities/styled_components/theming"
import { zIndexes } from "../../../styling/z_indexes"
import React from "react"
import styled, { css } from "styled-components"

const Container = styled.div`
	${themed(
		theme => css`
			${scStyles.box}
			background-color: ${theme.colors.shade.light};
		`,
	)}
`

const Sections = tagArg(
	styled(Container),
	themed(
		theme => css`
			position: relative;
			z-index: ${zIndexes.page.header};
			display: flex;
			align-items: center;
			justify-content: center;
			height: ${theme.sizes.page.header.height};
		`,
	),
)

export interface SectionModel {
	id: string
	label: string
	url: string
}

export const Header: React.FC<{
	sections: SectionModel[]
}> = ({ sections }) => (
	<Container>
		<Sections>
			{sections.map(section => (
				<Section key={section.id} section={section} />
			))}
		</Sections>
	</Container>
)
