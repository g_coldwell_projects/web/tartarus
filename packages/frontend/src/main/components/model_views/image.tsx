import { ImageModel } from "../../models/image"
import { componentForStyle } from "../../utilities/styled_components/common"
import { css } from "styled-components"
import React from "react"

const imageStyle = css<{
	objectFit?: "cover"
	imageWidth: string
	imageHeight: string
}>`
	${({ imageWidth, imageHeight }) =>
		`width: ${imageWidth}; height: ${imageHeight};`}
	${({ objectFit }) => `object-fit: ${objectFit ?? "cover"};`}
`

const Image = componentForStyle("img", imageStyle)

export const ImageView: React.FC<{
	image: ImageModel
	width?: string
	height?: string
}> = ({ image, width = "100%", height = "100%" }) => (
	<Image src={image.srcUrl} imageWidth={width} imageHeight={height} />
)
