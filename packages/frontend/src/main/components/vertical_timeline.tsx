import {
	VerticalTimeline as VerticalTimelineWrapped,
	VerticalTimelineElement as WrappedVerticalTimelineElement,
} from "react-vertical-timeline-component"
import { WithKey } from "../utilities/types/react"
import React, { ReactNode } from "react"
import styled from "styled-components"

const StyledVerticalTimelineWrapped = styled(VerticalTimelineWrapped)`
	width: 100%;
	height: 100%;
	padding: 0;
	margin: 0;
`

/* const TestWrapper = styled.div`
	width: 100%;
	display: flex;
	align-items: center;
	position: relative;
`

const TestLine = styled.div`
	height: 3px;
	margin: 0 2rem;
	background-color: red;
	flex-grow: 1;
`

const Test: React.FC = () => (
	<TestWrapper>
		<TestLine />
		Test
		<TestLine />
	</TestWrapper>
) */

interface Event {
	icon: ReactNode
	content: ReactNode
	date?: string
}

export const VerticalTimeline: React.FC<{
	events: WithKey<Event>[]
}> = ({ events }) => (
	<StyledVerticalTimelineWrapped>
		{events.map((event, index) => (
			<>
				{/* <Test /> */}
				<WrappedVerticalTimelineElement
					contentStyle={
						index == 0
							? {
									background: "rgb(33, 150, 243)",
									color: "#fff",
							  }
							: {
									color: "hsl(0deg, 0%, 0%)",
							  }
					}
					contentArrowStyle={
						index == 0
							? {
									borderRight: "7px solid  rgb(33, 150, 243)",
							  }
							: {}
					}
					key={event.key}
					icon={event.icon}
					date={event.date}
					className="test"
				>
					{event.content}
				</WrappedVerticalTimelineElement>
			</>
		))}
	</StyledVerticalTimelineWrapped>
)
