import { css } from "styled-components"

const montserrat = css`
	font-family: "Montserrat", sans-serif;
`

const staatliches = css`
	font-family: "Staatliches", cursive;
`

const roboto = {
	slab: css`
		font-family: "Staatliches";
	`,
}

export const scFonts = { montserrat, staatliches, roboto }
