import { componentForStyle } from "../../utilities/styled_components/common"
import { scStyles } from "./common_styles"

export const Box = componentForStyle("div", scStyles.box)
export const PageContent = componentForStyle("div", scStyles.pageContent)
export const Heading = {
	primary: componentForStyle("h1", scStyles.heading.primary),
	secondary: componentForStyle("h2", scStyles.heading.secondary),
}
export const Text = componentForStyle("div", scStyles.text)
export const Center = componentForStyle("div", scStyles.center)
