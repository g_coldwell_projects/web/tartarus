import { css } from "styled-components"
import { scFonts } from "./fonts"
import { themed, toSc } from "../../utilities/styled_components/theming"

const normalize = {
	link: themed(
		theme => css`
			text-decoration: none;
			transition: color ease-in-out 0.5s;
			color: ${theme.colors.text.link.normal};
			&:hover {
				color: ${theme.colors.text.link.hover};
			}
		`,
	),
}

const shadow = themed(
	theme => css`
		box-shadow: 0 3px 6px 0 ${theme.colors.shadow},
			0 3px 6px 0 ${theme.colors.shadow};
	`,
)

const box = themed(
	theme => css`
		${shadow}
		background-color: ${theme.colors.box.background};
	`,
)

const pageContent = themed(
	theme => css`
		width: 100%;
		overflow: auto;
		${toSc.compoundBoundingBox(theme.sizes.page.content.boundingBox)}
	`,
)

const center = css`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
`

const text = css`
	${scFonts.montserrat}
	font-size: 1rem;
`

const heading = {
	primary: css`
		${scFonts.roboto.slab}
		font-size: 2rem;
		font-weight: bold;
	`,
	secondary: css`
		${scFonts.roboto.slab}
		font-size: 1.5rem;
		font-weight: bold;
	`,
}

export const scStyles = {
	normalize,
	shadow,
	box,
	pageContent,
	center,
	text,
	heading,
}
