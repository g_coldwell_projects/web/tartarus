import { Sizes } from "../../theme/sizes"

export const sizes: Sizes = {
	page: {
		header: {
			height: "3rem",
			section: {
				boundingBox: {
					inner: {
						left: "calc(0.5rem + 2vw)",
						right: "calc(0.5rem + 2vw)",
						top: "0",
						bottom: "0",
					},
				},
			},
		},
		content: {
			boundingBox: {
				inner: {
					left: "calc(2rem + 1vw)",
					right: "calc(2rem + 1vw)",
					top: "3rem",
					bottom: "3rem",
				},
			},
		},
	},
}
