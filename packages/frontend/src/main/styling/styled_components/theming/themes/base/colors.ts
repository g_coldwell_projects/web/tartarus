import { Colors } from "../../theme/colors"

const coreColors = {
	white: "hsl(0deg, 0%, 100%)",
	lightGrey: "hsl(0deg, 0%, 95%)",
	lightBlue: "hsl(195deg, 100%, 50%)",
	blue: "hsl(205deg, 100%, 50%)",
}

export const colors: Colors = {
	page: {
		background: coreColors.lightGrey,
	},
	box: {
		background: coreColors.white,
	},
	text: {
		link: { normal: coreColors.lightBlue, hover: coreColors.blue },
	},
	shadow: "hsla(0deg, 0%, 0%, 0.15)", // TODO: Derive this from `black` color
	shade: {
		dark: "hsla(0deg, 0%, 0%, 0.05)", // TODO: Derive this from `black` color
		light: "hsla(0deg, 0%, 100%, 0.5)", // TODO: Derive this from `white` color
	},
}
