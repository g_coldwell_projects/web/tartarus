import { Theme } from "../../theme"
import { colors } from "./colors"
import { sizes } from "./sizes"

export const baseTheme: Theme = {
	colors,
	sizes,
}
