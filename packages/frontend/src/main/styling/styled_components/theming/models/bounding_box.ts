import { SizeDeclaration } from "../theme/sizes"

export interface BoundingBox {
	left: SizeDeclaration
	right: SizeDeclaration
	top: SizeDeclaration
	bottom: SizeDeclaration
}

export interface CompoundBoundingBox {
	inner?: BoundingBox
	outter?: BoundingBox
}
