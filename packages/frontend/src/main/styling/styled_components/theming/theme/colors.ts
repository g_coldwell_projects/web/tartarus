type ColorDefinition = string

export interface Colors {
	page: {
		background: ColorDefinition
	}
	box: {
		background: ColorDefinition
	}
	text: {
		link: { normal: ColorDefinition; hover: ColorDefinition }
	}
	shadow: ColorDefinition
	shade: {
		dark: ColorDefinition
		light: ColorDefinition
	}
}
