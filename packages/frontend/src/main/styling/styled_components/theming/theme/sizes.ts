import { CompoundBoundingBox } from "../models/bounding_box"

export type SizeDeclaration = string

export interface Sizes {
	page: {
		header: {
			height: SizeDeclaration
			section: {
				boundingBox: CompoundBoundingBox
			}
		}
		content: {
			boundingBox: CompoundBoundingBox
		}
	}
}
