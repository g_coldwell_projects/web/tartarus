import { Colors } from "./colors"
import { Sizes } from "./sizes"

export interface Theme {
	colors: Colors
	sizes: Sizes
}
