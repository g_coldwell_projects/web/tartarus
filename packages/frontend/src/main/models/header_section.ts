export interface HeaderSectionModel {
	id: string
	label: string
	url: string
}
