export interface ImageModel {
	srcUrl: string
	altText?: string
}
